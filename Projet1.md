# TP1 : Containers

# I. Docker

## 1. Install

🌞 **Installer Docker sur la machine**


```bash
[admin@localhost ~]$ docker --version
Docker version 24.0.7, build afdd53b
```

```bash
[admin@localhost nginx]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                           NAMES
6597cb42b99a   nginx     "/docker-entrypoint.…"   2 minutes ago   Up 2 minutes   80/tcp, 0.0.0.0:80->9999/tcp, :::80->9999/tcp   nostalgic_bartik
```

🌞 **Utiliser la commande `docker run`**

```bash
[admin@localhost ~]$ cat tp_docker/nginx/index.html
coucou bebou

[admin@localhost ~]$ cat tp_docker/nginx/admin.conf
server {
  listen 9999;
  root /var/www/tp_docker;
}

[admin@localhost nginx]$ docker run -v /home/admin/tp_docker/nginx/admin.conf:/etc/nginx/conf.d/admin.conf -v /home/admin/tp_docker/nginx/index.html:/var/www/tp_docker/index.html nginx


[admin@localhost nginx]$ docker run -v /home/admin/tp_docker/nginx/admin.conf:/etc/nginx/conf.d/admin.conf -v /home/admin/tp_docker/nginx/index.html:/var/www/tp_docker/index.html -p 80:9999 -d nginx

[admin@localhost nginx]$ docker run -v /home/admin/tp_docker/nginx/admin.conf:/etc/nginx/conf.d/admin.conf -v /home/admin/tp_docker/nginx/index.html:/var/www/tp_docker/index.html -p 80:9999 -m 512m --cpus=1 -d nginx
7de4cd37bcc7b96cb111b252cd9e0aa5e1ff0921b47d5bd7dfe9ffb7c6515912

[admin@localhost ~]$ docker inspect 7d | grep -i memory
            "Memory": 536870912,
            "MemoryReservation": 0,
            "MemorySwap": 1073741824,
            "MemorySwappiness": null,

```

## 2. Construisez votre propre Dockerfile

## II. Images

🌞 **Construire votre propre image**


```bash
[admin@localhost travail]$ cat Dockerfile
FROM ubuntu

RUN apt update -y

RUN apt install -y apach2
```





📁 **`Dockerfile`**

# III. `docker-compose`

## 1. Intro

`docker compose` est un outil qui permet de lancer plusieurs conteneurs en une seule commande.

> En plus d'être pratique, il fournit des fonctionnalités additionnelles, liés au fait qu'il s'occupe à lui tout seul de lancer tous les conteneurs. On peut par exemple demander à un conteneur de ne s'allumer que lorsqu'un autre conteneur est devenu "healthy". Idéal pour lancer une application après sa base de données par exemple.

Le principe de fonctionnement de `docker compose` :

- on écrit un fichier qui décrit les conteneurs voulus
  - c'est le `docker-compose.yml`
  - tout ce que vous écriviez sur la ligne `docker run` peut être écrit sous la forme d'un `docker-compose.yml`
- on se déplace dans le dossier qui contient le `docker-compose.yml`
- on peut utiliser les commandes `docker compose` :

```bash
# Allumer les conteneurs définis dans le docker-compose.yml
$ docker compose up
$ docker compose up -d

# Eteindre
$ docker compose down

# Explorer un peu le help, il y a d'autres commandes utiles
$ docker compose --help
```

La syntaxe du fichier peut par exemple ressembler à :

```yml
version: "3.8"

services:
  db:
    image: mysql:5.7
    restart: always
    ports:
      - '3306:3306'
    volumes:
      - "./db/mysql_files:/var/lib/mysql"
    environment:
      MYSQL_ROOT_PASSWORD: beep
      MYSQL_DATABASE: bip
      MYSQL_USER: bap
      MYSQL_PASSWORD: boop

  nginx:
    image: nginx
    ports:
      - "80:80"
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf:ro
    restart: unless-stopped
```

> Pour connaître les variables d'environnement qu'on peut passer à un conteneur, comme `MYSQL_ROOT_PASSWORD` au dessus, il faut se rendre sur la doc de l'image en question, sur le Docker Hub par exemple.

## 2. WikiJS

WikiJS est une application web plutôt cool qui comme son nom l'indique permet d'héberger un ou plusieurs wikis. Même principe qu'un MediaWiki donc (solution opensource utilisée par Wikipedia par exemple) mais avec un look plus moderne.

🌞 **Installez un WikiJS** en utilisant Docker

- WikiJS a besoin d'une base de données pour fonctionner
- il faudra donc deux conteneurs : un pour WikiJS et un pour la base de données
- référez-vous à la doc officielle de WikiJS, c'est tout guidé

🌞 **Construisez vous-mêmes l'image de WikiJS**

- récupérez le Dockerfile officiel et utilisez-le pour build localement l'image
- intégrez un build automatique de l'image dans le `docker-compose.yml` (ça se fait avec la clause `build:`)
- assurez-vous, pour des raisons de sécurité, qu'un autre utilisateur que `root` est utilisé

## 3. Make your own meow

Pour cette partie, vous utiliserez une application à vous que vous avez sous la main.

N'importe quelle app fera le taff, un truc dév en cours, en temps perso, au taff, trouvée sur internet, une app web, un serveur de jeu. Peu importe. Ce serait quand même plus cool s'il y a besoin d'au moins deux conteneurs histoire d'exploiter un peu `docker compose`.

Peu importe le langage aussi ! Go, Python, PHP (désolé des gros mots), NodeJS (j'ai déjà dit désolé pour les gros mots ?), ou autres.

🌞 **Conteneurisez votre application**

- créer un `Dockerfile` maison qui porte l'application
- créer un `docker-compose.yml` qui permet de lancer votre application
- vous préciserez dans le rendu les instructions pour lancer l'application
  - indiquer la commande `git clone` pour récupérer votre dépôt
  - le `cd` dans le bon dossier
  - la commande `docker build` pour build l'image
  - la commande `docker-compose` pour lancer le(s) conteneur(s)
  - comme un vrai README qui m'explique comment lancer votre app !

📁 📁 `app/Dockerfile` et `app/docker-compose.yml`. Je veux un sous-dossier `app/` sur votre dépôt git avec ces deux fichiers dedans :)

# IV. Docker security

Dans cette partie, on va survoler quelques aspects de Docker en terme de sécurité.

## 1. Le groupe docker

Si vous avez correctement ajouté votre utilisateur au groupe `docker`, vous utilisez normalement Docker sans taper aucune commande `sudo`.

> La raison technique à ça c'est que vous communiquez avec Docker en utilisant le socket `/var/run/docker.sock`. Demandez-moi si vous voulez + de détails sur ça.

Cela découle sur le fait que vous avez les droits `root` sur la machine. Sans utiliser aucune commande `sudo`, sans devenir `root`, sans même connaître son mot de passe ni rien, si vous êtes membres du groupe `docker` vous pouvez devenir `root` sur la machine.

🌞 **Prouvez que vous pouvez devenir `root`**

- en étant membre du groupe `docker`
- sans taper aucune commande `sudo` ou `su` ou ce genre de choses
- normalement, une seule commande `docker run` suffit
- pour prouver que vous êtes `root`, plein de moyens possibles
  - par exemple un `cat /etc/shadow` qui contient les hash des mots de passe de la machine hôte

## 2. Scan de vuln

Il existe des outils dédiés au scan de vulnérabilités dans des images Docker.

C'est le cas de [Trivy](https://github.com/aquasecurity/trivy) par exemple.

🌞 **Utilisez Trivy**

- effectuez un scan de vulnérabilités sur des images précédemment mises en oeuvre :
  - celle de WikiJS que vous avez build
  - celle de sa base de données
  - l'image de Apache que vous avez build
  - l'image de NGINX officielle utilisée dans la première partie

## 3. Petit benchmark secu

Il existe plusieurs référentiels pour sécuriser une machine donnée qui utilise un OS donné. Un savoir particulièrement recherché pour renforcer la sécurité des serveurs surtout.

Un des référentiels réputé et disponible en libre accès, ce sont [les benchmarks de CIS](https://www.cisecurity.org/cis-benchmarks). Ce sont ni plus ni moins que des guides complets pour sécuriser de façon assez forte une machine qui tourne par exemple sous Debian, Rocky Linux ou bien d'autres.

[Docker développe un petit outil](https://github.com/docker/docker-bench-security) qui permet de vérifier si votre utilisation de Docker est compatible avec les recommandations de CIS.

🌞 **Utilisez l'outil Docker Bench for Security**

- rien à me mettre en rendu, je vous laisse exprimer votre curiosité quant aux résultats
- ce genre d'outils est cool d'un point de vue pédagogique : chaque check que fait le script c'est un truc à savoir finalement !